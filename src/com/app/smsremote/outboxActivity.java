package com.app.smsremote;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class outboxActivity extends Activity {

	TextView result;
	static String outbox_file="outbox_file";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.inbox);
		result = (TextView)findViewById(R.id.text);
//		new getOutboxMessage().execute(new String[]{getSharedPreferences("smsremotesettings", Context.MODE_PRIVATE).getString("outbox", "")});
		result.setText("Loading");
		
		    try {
		    	BufferedReader input = new BufferedReader(new InputStreamReader(openFileInput(outbox_file)));
			    String line;
			    StringBuffer buffer = new StringBuffer();
				while ((line = input.readLine()) != null) {
				buffer.append(line);
				}
				result.setText(buffer.toString());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	
	
	
}
