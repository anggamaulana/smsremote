package com.app.smsremote;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

public class schedulerReceiver extends BroadcastReceiver {

	static String outbox_file="outbox_file";
	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
//		Toast.makeText(context, "Updating sms outbox", Toast.LENGTH_LONG).show();
		new getOutboxMessage(context).execute(new String[]{context.getSharedPreferences("smsremotesettings", Context.MODE_PRIVATE).getString("outbox", "")});
	}
	
	public class getOutboxMessage extends AsyncTask<String,Void, String>{
		String resp="";
		Context con;
		SmsManager smsman;
		
		public getOutboxMessage(Context con){
			this.con=con;
			 smsman = SmsManager.getDefault();
		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			Log.d("scheduleroutbox", "outbox alarm");
			String outbox = params[0];
			if(outbox.equals("")){
				
				return "alamat tidak valid";
			}
			HttpClient client = new DefaultHttpClient();
			try {
				
				
				
				
				HttpResponse response = client.execute(new HttpGet(outbox));
				BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
				String s="";
				while((s=br.readLine())!=null){
					resp+=s+"\n";
				}
				Log.d("scheduleroutbox", "outbox alarm send sms if any");
				sendSMS(resp);
				
				FileOutputStream fl = con.openFileOutput(outbox_file, Context.MODE_APPEND);
				
				fl.write(resp.getBytes());
				fl.close();
				
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return resp;
		}
		
		private void sendSMS(String resp2) {
			// TODO Auto-generated method stub
			Pattern pt = Pattern.compile("<DestinationNumber>[0-9]+</DestinationNumber>");
			Matcher match = pt.matcher(resp2);
			Pattern pt2 = Pattern.compile("<TextDecoded>([\\w\\W]+?)</TextDecoded>");
			Matcher match2 = pt2.matcher(resp2);
			List<String> Number = new ArrayList<String>();
			List<String> Text = new ArrayList<String>();
			while(match.find()){
				String matched=match.group(0).replace("<DestinationNumber>", "").replace("</DestinationNumber>", "");
				Number.add(matched);
				Log.d("scheduleroutbox", "outbox "+matched);
			}
			while(match2.find()){
				String matched=match2.group(1).replace("<TextDecoded>", "").replace("</TextDecoded>", "");
				Text.add(matched);
				Log.d("scheduleroutbox", "outbox "+matched);
			}
			
			for(int i=0;i<Number.size();i++){
				//sms
				Log.d("scheduleroutbox", "sending sms to "+Number.get(i));
				
				smsman.sendTextMessage(Number.get(i), null, Text.get(i), null, null);
//				Toast.makeText(con,"Sms to"+Number.get(i)+"sent",Toast.LENGTH_SHORT).show();
			}
			
		}

		@Override
		protected void onProgressUpdate(Void... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
		}
		
		@Override
		protected void onPostExecute(String res) {
			// TODO Auto-generated method stub
			super.onPostExecute(res);
			
		}
		
	}

}
