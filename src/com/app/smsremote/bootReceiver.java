package com.app.smsremote;

import java.util.Calendar;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class bootReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context co, Intent arg1) {
		// TODO Auto-generated method stub
		setScheduler(co);
	}
	
	private void setScheduler(Context co){
		long interval = co.getSharedPreferences("smsremotesettings", Context.MODE_PRIVATE).getInt("interval", 300)*1000;
		 Intent downloader = new Intent(co, schedulerReceiver.class);
         downloader.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
         PendingIntent pendingIntent = PendingIntent.getBroadcast(co, 0, downloader, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager alarmManager = (AlarmManager)co.getSystemService(Context.ALARM_SERVICE);
       alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,Calendar.getInstance().getTimeInMillis(),interval, pendingIntent);
	}

}
