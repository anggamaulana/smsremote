package com.app.smsremote;

import java.util.Calendar;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

public class settingActivity extends Activity implements OnClickListener{

	TextView interval,inbox,outbox,hp;
	CheckBox bls;
	Button setting;
	SharedPreferences pref;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings);
		interval = (TextView)findViewById(R.id.txt_interval);
		inbox = (TextView)findViewById(R.id.txt_url_inbox);
		outbox = (TextView)findViewById(R.id.txt_url_outbox);
		bls=(CheckBox)findViewById(R.id.cek_balasan_server);
		pref = getSharedPreferences("smsremotesettings", Context.MODE_PRIVATE);
		
		
		interval.setText(String.valueOf(pref.getInt("interval",10000)));
		inbox.setText(pref.getString("inbox",""));
		outbox.setText(pref.getString("outbox",""));
		bls.setChecked(pref.getBoolean("balasanserver", false));
		setting = (Button)findViewById(R.id.btn_setting_save);
		setting.setOnClickListener(this);
		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int id=v.getId();
		if(id==R.id.btn_setting_save){
			
			Editor ed = pref.edit();
			ed.putInt("interval", Integer.parseInt(interval.getText().toString()));
			ed.putString("inbox", inbox.getText().toString());
			ed.putString("outbox", outbox.getText().toString());
			if(bls.isChecked()){
				ed.putBoolean("balasanserver", true);
			}else{
				ed.putBoolean("balasanserver", false);
			}
			ed.commit();
			setScheduler();
			finish();
		}
	}
	
	private void setScheduler(){
		long interval = getSharedPreferences("smsremotesettings", Context.MODE_PRIVATE).getInt("interval", 300)*1000;
		 Intent downloader = new Intent(this, schedulerReceiver.class);
         downloader.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
         PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, downloader, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
       alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,Calendar.getInstance().getTimeInMillis(),interval, pendingIntent);
	}
}
