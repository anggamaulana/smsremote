package com.app.smsremote;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class serverResponseActivity extends Activity implements OnClickListener{
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.response);
		String resp = getIntent().getStringExtra("resp");
		TextView res = (TextView)findViewById(R.id.txt_server_response);
		Button btn=(Button)findViewById(R.id.btn_response_close);
		btn.setOnClickListener(this);
		res.setText(resp);
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		if(arg0.getId()==R.id.btn_response_close){
			finish();
		}
	}

}
