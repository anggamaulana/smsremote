package com.app.smsremote;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;



import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

public class smsReceiver extends BroadcastReceiver {
    
    // Get the object of SmsManager
    final SmsManager sms = SmsManager.getDefault();
    static String inbox_file="inbox_file";
     
    public void onReceive(Context context, Intent intent) {
     
        // Retrieves a map of extended data from the intent.
        final Bundle bundle = intent.getExtras();
 
        try {
             
            if (bundle != null) {
                 
                final Object[] pdusObj = (Object[]) bundle.get("pdus");
                 
                for (int i = 0; i < pdusObj.length; i++) {
                     
                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    String phoneNumber = currentMessage.getDisplayOriginatingAddress();
                     
                    String senderNum = phoneNumber;
                    senderNum = senderNum.replace("+62", "0");
                    String message = currentMessage.getDisplayMessageBody();
 
                    Log.i("SmsReceiver", "senderNum: "+ senderNum + "; message: " + message);
                    
                 
                    Toast.makeText(context, 
                                 "Sending sms to server \nsenderNum: "+ senderNum + ", message: " + message, Toast.LENGTH_LONG).show();
                    
                    
                    
                    new sendInboxMessage(context).execute(
                    		new String[]{context.getSharedPreferences("smsremotesettings", Context.MODE_PRIVATE).getString("inbox", ""),senderNum,message});
            		Thread.sleep(2000);
            		Log.i("SmsReceiver", "delete sms");
                    //delete sms
                    Uri uriSms = Uri.parse("content://sms/");
                    Cursor c = context.getContentResolver().query(uriSms,
                        new String[] { "_id", "thread_id", "address","person", "date", "body" }, null, null, null);
                    if (c != null && c.moveToFirst()) {

                            long id = c.getLong(0);
                            Log.i("SmsReceiver", "delete"+c.getString(5));
                            context.getContentResolver().delete(Uri.parse("content://sms/" + id), null, null);

                    }
                   
                     
                } // end for loop
              } // bundle is null
 
        } catch (Exception e) {
            Log.e("SmsReceiver", "Exception smsReceiver" +e);
             
        }
    }   
    
   
    
    public class sendInboxMessage extends AsyncTask<String,Void, String>{
		String resp="";
		Context con;
		SmsManager smsman;
		
		public sendInboxMessage(Context co){
			this.con=co;
			smsman=SmsManager.getDefault();
		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String inbox = params[0];
			String nohp = params[1];
			String text = params[2];
			resp+="Pengirim:"+nohp+" | Text:"+text+"\n\n";
			if(inbox.equals("")){
				
				return "alamat tidak valid";
			}
			HttpClient client = new DefaultHttpClient();
			try {
				HttpPost post = new HttpPost(inbox);
				List<NameValuePair> data = new ArrayList<NameValuePair>(2);
				
				//DATA POST
//				MCrypt mcrypt = new MCrypt();
//
//				/* Encrypt */
//
//				String encrypted_hp = MCrypt.bytesToHex( mcrypt.encrypt(nohp) );
//				String encrypted_txt = MCrypt.bytesToHex( mcrypt.encrypt(text) );
//				
//				data.add(new BasicNameValuePair("num", encrypted_hp));
//				data.add(new BasicNameValuePair("text", encrypted_txt));
				
				data.add(new BasicNameValuePair("num", nohp));
				data.add(new BasicNameValuePair("text", text));
				post.setEntity(new UrlEncodedFormEntity(data));
//				client.execute(post);
				Log.d("scheduleroutbox", "inbox receive");
				HttpResponse response = client.execute(post);
				BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
				String s="";
				while((s=br.readLine())!=null){
					resp+=s+"\n";
				}
				if(con.getSharedPreferences("smsremotesettings", Context.MODE_PRIVATE).getBoolean("balasanserver", false)){
					Log.d("scheduleroutbox", "inbox reply to");
					
					sendSMS(resp);
				}
				
				FileOutputStream fl = con.openFileOutput(inbox_file, Context.MODE_APPEND);
				
				fl.write(resp.getBytes());
				fl.close();
				
				
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return resp;
		}
		
		@Override
		protected void onProgressUpdate(Void... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
		}
		
		private void sendSMS(String resp2) {
			// TODO Auto-generated method stub
			Pattern pt = Pattern.compile("<DestinationNumber>[0-9]+</DestinationNumber>");
			Matcher match = pt.matcher(resp2);
			Pattern pt2 = Pattern.compile("<TextDecoded>([\\w\\W]+?)</TextDecoded>");
			Matcher match2 = pt2.matcher(resp2);
			List<String> Number = new ArrayList<String>();
			List<String> Text = new ArrayList<String>();
			while(match.find()){
				String matched=match.group(0).replace("<DestinationNumber>", "").replace("</DestinationNumber>", "");
				Number.add(matched);
				Log.d("replyinbox", "inbox num"+matched);
			}
			while(match2.find()){
				String matched=match2.group(1).replace("<TextDecoded>", "").replace("</TextDecoded>", "");
				Text.add(matched);
				Log.d("replyinbox", "inbox text"+matched);
			}
			
			for(int i=0;i<Number.size();i++){
				//sms
				Log.d("replyinbox", "sending sms to "+Number.get(i));
				
				smsman.sendTextMessage(Number.get(i), null, Text.get(i), null, null);
//				Toast.makeText(con,"Sms to"+Number.get(i)+"sent",Toast.LENGTH_SHORT).show();
			}
			
		}
		
		@Override
		protected void onPostExecute(String res) {
			// TODO Auto-generated method stub
			super.onPostExecute(res);
			
		}
		
		
		
	}
}