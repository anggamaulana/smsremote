package com.app.smsremote;

import java.util.Calendar;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity implements OnClickListener {

	SharedPreferences pref;
	Button inbox,outbox,setting;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		pref = getSharedPreferences("smsremotesettings", Context.MODE_PRIVATE);
		
		String cek = pref.getString("inbox","");
		if(cek.equals("")){
			Editor ed = pref.edit();
			ed.putInt("interval",10);
			ed.putString("inbox", "http://119.235.252.54/sms_remote/inbox.php?pref=ALL");
			ed.putString("outbox", "http://119.235.252.54/sms_remote/outbox.php?pref=ALL");
			ed.putBoolean("balasanserver", true);
			ed.commit();
		}
		inbox = (Button)findViewById(R.id.btninbox);
		outbox = (Button)findViewById(R.id.btnoutbox);
		setting = (Button)findViewById(R.id.btn_settings);
		inbox.setOnClickListener(this);
		outbox.setOnClickListener(this);
		setting.setOnClickListener(this);
		setScheduler();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int id=v.getId();
		if(id==R.id.btninbox){
			startActivity(new Intent(this,inboxActivity.class));
			
		}else if(id==R.id.btnoutbox){
			startActivity(new Intent(this,outboxActivity.class));
		}else if(id==R.id.btn_settings){
			startActivity(new Intent(this,settingActivity.class));
		}
	}
	
	
	private void setScheduler(){
		long interval = getSharedPreferences("smsremotesettings", Context.MODE_PRIVATE).getInt("interval", 300)*1000;
		 Intent downloader = new Intent(this, schedulerReceiver.class);
         downloader.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
         PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, downloader, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
       alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,Calendar.getInstance().getTimeInMillis(),interval, pendingIntent);
	}

}
