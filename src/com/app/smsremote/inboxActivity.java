package com.app.smsremote;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class inboxActivity extends Activity {
	TextView result;
	static String inbox_file="inbox_file";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.inbox);
		result = (TextView)findViewById(R.id.text);
//		new getInboxMessage().execute(new String[]{getSharedPreferences("smsremotesettings", Context.MODE_PRIVATE).getString("inbox", ""),getSharedPreferences("smsremotesettings", Context.MODE_PRIVATE).getString("nohp", "")});
		result.setText("Loading");
		
		
	    try {
	    	BufferedReader input = new BufferedReader(new InputStreamReader(openFileInput(inbox_file)));
		    String line;
		    StringBuffer buffer = new StringBuffer();
			while ((line = input.readLine()) != null) {
			buffer.append(line);
			}
			result.setText(buffer.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
